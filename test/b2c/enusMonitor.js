const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {click,getText,getCount,shouldExist,waitForText} = require('../../lib/helper')

describe('B2C EN-US Monitor - All Monitors',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to All monitor Page - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //要點六次+
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        await page.click('#seriesproducts > div.right > div.more > i')
        await page.waitForTimeout(5000)//等待5000毫秒
        const allMontior = " #seriesproducts > div.right > ul.products > ul > li"
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const countallMontior = await getCount(page, allMontior)
        console.log("Total of All Montior:",countallMontior)
        expect(countallMontior).to.equal(54)//目前Monitor有54個產品
    })
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(1) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(2) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(3) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(4) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(5) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(6) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(7) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(8) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(9) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(10) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(11) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(12) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(13) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(14) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(15) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(16) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(17) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(18) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(19) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(20) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(21) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(22) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(23) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(24) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(25) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(26) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(27) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(28) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(29) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(30) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(31) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(32) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(33) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(34) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(35) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(36) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(37) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(38) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(39) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(40) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(41) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(42) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(43) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(44) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(45) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(46) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(47) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(48) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(49) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(50) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(51) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(52) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(53) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor.html')
        //第一個projector
        var getallMontiorURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(54) > a"
        var allMontiorProductURL = await page.$eval(getallMontiorURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allMontiorURL= allMontiorProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allMontiorSpecURL = "https://www.benq.com/"+allMontiorURL
        console.log(allMontiorSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allMontiorSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})

describe('B2C EN-US Monitor - All Software',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to monitor Page- Software(DesignVue Monitor PD Series) - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/monitor/software.html')
        await page.waitForTimeout(5000)//等待5000毫秒
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const allSoftwareDesignVue = "body > div:nth-child(12) > div > div > div.peoplelisting > div > div"
        const countallSoftwareDesignVue = await getCount(page, allSoftwareDesignVue)
        console.log("Total of All Software(DesignVue Monitor PD Series):",countallSoftwareDesignVue)
        expect(countallSoftwareDesignVue).to.equal(1)//目前Software(DesignVue Monitor PD Series)有1個產品
    })
    it('Go to monitor Page- Software(PhotoVue Monitor SW Series) - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/monitor/software.html')
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const allSoftwarePhotoVue = "body > div:nth-child(13) > div > div > div.peoplelisting > div > div"
        const countallSoftwarePhotoVue = await getCount(page, allSoftwarePhotoVue)
        console.log("Total of All Software(PhotoVue Monitor SW Series):",countallSoftwarePhotoVue)
        expect(countallSoftwarePhotoVue).to.equal(2)//目前Software(PhotoVue Monitor SW Series)有1個產品
    })
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor/software.html')
        //第一個software
        var getallSoftwareURL = "body > div:nth-child(12) > div > div > div.peoplelisting > div > div > a"
        var allSoftwareProductURL = await page.$eval(getallSoftwareURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allSoftwareURL= allSoftwareProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allSoftwareSpecURL = "https://www.benq.com/"+allSoftwareURL
        console.log(allSoftwareSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allSoftwareSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor/software.html')
        await page.waitForTimeout(5000)//等待5000毫秒
        //第一個software
        var getallSoftwareURL = "body > div:nth-child(13) > div > div > div.peoplelisting > div > div:nth-child(1) > a"
        var allSoftwareProductURL = await page.$eval(getallSoftwareURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allSoftwareURL= allSoftwareProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allSoftwareSpecURL = "https://www.benq.com/"+allSoftwareURL
        console.log(allSoftwareSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allSoftwareSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor/software.html')
        await page.waitForTimeout(5000)//等待5000毫秒
        //第一個software
        var getallSoftwareURL = "body > div:nth-child(13) > div > div > div.peoplelisting > div > div:nth-child(2) > a"
        var allSoftwareProductURL = await page.$eval(getallSoftwareURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allSoftwareURL= allSoftwareProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allSoftwareSpecURL = "https://www.benq.com/"+allSoftwareURL
        console.log(allSoftwareSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allSoftwareSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
})

describe('B2C EN-US Monitor - All Accessories',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            // executablePath:
            // "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            executablePath:
            "./node_modules/puppeteer/.local-chromium/win32-901912/chrome-win/chrome",
            headless:true,//無介面模式:有無需要開視窗,false要開,true不開
            slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        //設定像素
        await page.setViewport({width:1200,height:1000})

        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    it('Go to monitor Page- all accessories - Check number',async function(){
        await page.goto('https://www.benq.com/en-us/monitor/accessory.html')
        //step2:計算每一個Series Page數量(ex:假設現在是10個, 之後數量如果增加了, 腳本就要改) 
        const allAccessories= "#seriesproducts > div.right > ul.products > ul > li"
        const countallAccessories = await getCount(page, allAccessories)
        console.log("Total of all Accessories:",countallAccessories)
        expect(countallAccessories).to.equal(2)//目前Software(DesignVue Monitor PD Series)有1個產品
    })
    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor/accessory.html')
        //第一個software
        var getallAccessoriesURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(1) > a"
        var allAccessoriesProductURL = await page.$eval(getallAccessoriesURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allAccessoriesURL= allAccessoriesProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allAccessoriesSpecURL = "https://www.benq.com/"+allAccessoriesURL
        console.log(allAccessoriesSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allAccessoriesSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })

    it('check each Spec exist or not',async function(){
        //step3:取得Series Page上的每一個Product Card 的URL
        await page.goto('https://www.benq.com/en-us/monitor/accessory.html')
        //第一個software
        var getallAccessoriesURL = "#seriesproducts > div.right > ul.products > ul > li:nth-child(2) > a"
        var allAccessoriesProductURL = await page.$eval(getallAccessoriesURL, element=> element.getAttribute("href"))
        // console.log(gamingProjectorProductURL)
        //step4:去除.html然後加上/specifications.html
        var allAccessoriesURL= allAccessoriesProductURL.replace(".html","/specifications.html")
        //console.log(gamingProjectorURL)
        var allAccessoriesSpecURL = "https://www.benq.com/"+allAccessoriesURL
        console.log(allAccessoriesSpecURL)
        //step5:改好URL以後, 在各自前往此page
        await page.goto(allAccessoriesSpecURL)
        //step6:檢查spec是否空白(是否存在".block")
        await shouldExist(page,'.block')
    })
    
})